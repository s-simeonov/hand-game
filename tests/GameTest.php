<?php 

class GameTest extends \PHPUnit_Framework_TestCase
{
	protected $game;

	public function setUp()
	{
		$game = new \Game\Game();

		list($rock, $paper, $scissors) = [new \GameContent\Hand("Rock"), new \GameContent\Hand("Paper"), new \GameContent\Hand("Scissors")];

		$rock 		->winsAgainst($scissors);
		$paper 		->winsAgainst($rock);
		$scissors 	->winsAgainst($paper);		
		
		$game
			->addHand($rock)
			->addHand($paper)
			->addHand($scissors)
			
			->addPlayer(new \GameContent\Player("Ivan"))
			->addPlayer(new \GameContent\Player("Georgi"))
			->addPlayer(new \GameContent\Player("Misho"));

		$this->game = $game;
	}

	public function testIfAddingInvalidHandsTrowsException()
	{
		$this->setExpectedException(\Exception::class, "Invalid hand. Each hand must have atleast one win against!");
		$game = new \Game\Game();
		$rock = new \GameContent\Hand("Rock");		
		$game->addHand($rock);
	}	

	public function testIfStartingGameWithoutPlayersThrowsException()
	{
		$this->setExpectedException(\Exception::class, "Not enough players!");
		$game = new \Game\Game();
		$game->playRounds(4);
	}

	public function testIfStartingGameWithoutEnoughHandsThrowsException()
	{
		$this->setExpectedException(\Exception::class, "Not enough hands!");
		$game = new \Game\Game();
		$game->addPlayer(new \GameContent\Player("Ivan"));
		
		$game->playRounds(4);

		$rock = new \GameContent\Hand("Rock");
		$scissors = new \GameContent\Hand("Scissors");	
		$rock->winsAgainst($scissors);

		$game->addHand($rock);

		$game->playRounds(4);

	}

	public function testIfStartingWithIncorrectRoundsThrowsException()
	{
		$this->setExpectedException(\Exception::class, "Incorrect rounds!");
		$this->game->playRounds(-5);		
	}		

	public function testIfPlayRoundsProducesAWinner()
	{
		$this->game->playRounds(4);
		$this->isInstanceOf(\GameContent\Player::class, $this->game->winner());
	}		
}

