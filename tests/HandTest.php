<?php 

class HandTest extends \PHPUnit_Framework_TestCase
{
	public function testIsTypeTrimmedAndLowered()
	{
		$rock = new \GameContent\Hand(" Rock   ");
		$this->assertEquals($rock->getType(), "rock");
	}

	public function testIfHandIsValid()
	{
		$rock = new \GameContent\Hand("rock");
		$scissors = new \GameContent\Hand("scissors"); 

		$this->assertFalse($rock->isValid());

		$rock->winsAgainst($scissors);

		$this->assertTrue($rock->isValid());
	}

	public function testIfWinsAgainstSingleHand()
	{
		$rock = new \GameContent\Hand("rock");
		$scissors = new \GameContent\Hand("scissors");

		$this->assertFalse($rock->checkIfWinsAgainst($scissors));
		
		$rock->winsAgainst($scissors);	

		$this->assertTrue($rock->checkIfWinsAgainst($scissors));

	}

	public function testIfWinsAgainstMultipleHands()
	{
		$rock = new \GameContent\Hand("rock");
		$scissors = new \GameContent\Hand("scissors");
		$wood = new \GameContent\Hand("wood");

		$this->assertFalse($rock->checkIfWinsAgainst($scissors));
		$this->assertFalse($rock->checkIfWinsAgainst($wood));

		$rock->winsAgainst($scissors);	
		$rock->winsAgainst($wood);

		$this->assertTrue($rock->checkIfWinsAgainst($scissors));
		$this->assertTrue($rock->checkIfWinsAgainst($wood));

	}

	public function testThatHandCantWinAgainstItself()
	{
		$rock = new \GameContent\Hand("rock");
		$rock->winsAgainst($rock);

		$this->assertFalse($rock->checkIfWinsAgainst($rock));
	}

	public function testThatTwoHandsCantWinAgainstEachotherAtTheSameTime()
	{
		$rock = new \GameContent\Hand("rock");
		$scissors = new \GameContent\Hand("scissors");

		$rock->winsAgainst($scissors);	
		$scissors->winsAgainst($rock);

		$this->assertTrue($rock->checkIfWinsAgainst($scissors));
		$this->assertFalse($scissors->checkIfWinsAgainst($rock));
	}	
}

