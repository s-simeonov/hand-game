<?php 

class PlayerTest extends \PHPUnit_Framework_TestCase
{
	public function testIfGetNameWorks()
	{
		$ivan = new \GameContent\Player("Ivan");
		$this->assertEquals($ivan->getName(), "Ivan");
	}

	public function testIfPlayerStartsWithZeroScore()
	{
		$ivan = new \GameContent\Player("Ivan");
		$this->assertEquals($ivan->getScore(), 0);		
	}

	public function testIfPlayerScoreCanBeIncreased()
	{
		$ivan = new \GameContent\Player("Ivan");
		
		$this->assertEquals($ivan->getScore(), 0);	
		$ivan->increaseScore();
		$this->assertEquals($ivan->getScore(), 1);
		$ivan->increaseScore();
		$ivan->increaseScore();
		$ivan->increaseScore();
		$this->assertEquals($ivan->getScore(), 4);
	}

	public function testIfPlayerCanDrawHands()
	{
		$ivan = new \GameContent\Player("Ivan");
		$hands = [new \GameContent\Hand("Rock"), new \GameContent\Hand("Paper")];

		$ivan->drawHand($hands);

		$this->assertNotNull($ivan->getHand());
		$this->assertInstanceOf(\GameContent\Hand::class, $ivan->getHand());
	}
}

