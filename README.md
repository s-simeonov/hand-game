Hand Game
========================

This is a small php application, based on the rock, paper scissors game.

Usage
---------------

### Running normally

Run the src/index.php file

### Running the unit tests

Run Composer:-

```
composer install
composer dump-autoload -o
```

You will then be able to run the unit tests using:-

```
phpunit
```