<?php 
use Game\Game;
use GameContent\Player;
use GameContent\Hand;

spl_autoload_register(function ($className) {
    require_once "{$className}.php";
});

list($rock, $paper, $scissors, $water) = [new Hand("Rock"), new Hand("Paper"), new Hand("Scissors"), new Hand("Water")];

$rock 		->winsAgainst($scissors);
$paper 		->winsAgainst($rock);
$scissors 	->winsAgainst($paper);
$water 		->winsAgainst($paper);
$water 		->winsAgainst($scissors);

$game = new Game();
$game
	->addHand($rock)
	->addHand($paper)
	->addHand($scissors)
	->addHand($water)
		
	->addPlayer(new Player("Ivan"))
	->addPlayer(new Player("Georgi"))
	->addPlayer(new Player("Misho"));


$game->playRounds(4);

echo $game->winner();

?>