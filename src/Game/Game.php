<?php  
namespace Game;
use GameContent\Player;
use GameContent\Hand;

class Game
{

	private $total_rounds 	= 0;
	private $current_round 	= 0;
	private $players 		= [];
	private $hands 			= [];
	private $winning_hand;
	private $winner;
	private $highest_score 	= 0;
	
	public function playRounds(int $rounds=0)
	{
		$this->total_rounds = $rounds;
		$this->validateInput();

		while ($this->current_round < $rounds) {
			$this->writeLine("============= Round: " . $this->current_round . "===================");
			$this->play();
			$this->current_round++;
		}

		$this->calculateGameWinner();

	}

	private function validateInput()
	{
		if( empty($this->players) ){
			throw new \Exception("Not enough players!");
		}

		if( count($this->hands) < 2){
			throw new \Exception("Not enough hands!");	
		}

		if(!is_int($this->total_rounds) || $this->total_rounds<1){
			throw new \Exception("Incorrect rounds!");
		}				
	}

	public function play(array $players = [])
	{
		$players = !empty($players) ? $players : $this->players;

		$this->drawPlayerHands($players);
		$this->calculateWinningHand($players);
		$this->calculateRoundWinner($players);

	}

	private function drawPlayerHands(array $players = [])
	{
		$players = !empty($players) ? $players : $this->players;

		foreach ($players as $player) {
			$player->drawHand($this->hands);
			$this->writeLine($player->getName() . " drew " . $player->getHand()->getType());
		}
	}

	private function calculateWinningHand(array $players = [])
	{
		$players = !empty($players) ? $players : $this->players;

		unset($this->winning_hand);

		//hand is winning if it ties or wins against all the remaining hands
		for($i=0; $i<count($players); $i++){

			$hand = $players[$i]->getHand();
			$is_winning = true;

			for($j=0; $j<count($players); $j++){
				$compared_hand = $players[$j]->getHand();
				if($i===$j || $hand->getType() === $compared_hand->getType()){
					continue;
				}
				if(!$hand->checkIfWinsAgainst($compared_hand)){
					$is_winning = false;
					break;
				}
			}	

			if ($is_winning) {
				$this->winning_hand = $hand;
				break;
			}								
		}

	}

	private function calculateRoundWinner(array $players = [])
	{
		$players = !empty($players) ? $players : $this->players;

		if(isset($this->winning_hand)){
			$this->writeLine("Winning Hand: " . $this->winning_hand->getType());

			$winners = [];

			foreach ($players as $player) {
				if( $player->getHand()->getType() === $this->winning_hand->getType()){
					$winners[] = $player;
				}
			}	

			if(count($winners)>1){
				$this->writeLine("Tied winners (" . implode(", ", $winners) . "). They will play again.");
				$this->writeLine("**************");				
				$this->play($winners);
			}
			else{
				$this->writeLine("Winner is: " . $winners[0]->getName());
				$winners[0]->increaseScore();
				if($winners[0]->getScore() > $this->highest_score){
					$this->highest_score = $winners[0]->getScore(); 
				}
			}

		}
		else{
			$this->writeLine("No winning hand. Drawing again");
			$this->writeLine("----------------");
			$this->play($players);
		}

	}

	private function calculateGameWinner(array $players = [])
	{
		$this->writeLine("############################### Player scores #########################################");
		$this->writeLine("Highest score is: ". $this->highest_score);

		$players = !empty($players) ? $players : $this->players;

		$winners = [];

		foreach ($players as $player) {
			$this->writeLine( $player->getName() . ": ". $player->getScore() );
			if($player->getScore() == $this->highest_score){
				$winners[] = $player;
			}	
		}

		if(count($winners)>1){
			$this->writeLine("Tied winners (" . implode(", ", $winners) . "). They will play again.");
			$this->writeLine("**************");				
			$this->play($winners);
			$this->calculateGameWinner($winners);
		}
		else{
			$this->winner = $winners[0];	
		}
	}

	public function winner() : Player
	{
		return $this->winner;
	}

	public function addPlayer(Player $player) : Game
	{
		$this->players[]=$player;
		return $this;
	}

	public function addHand(Hand $hand) : Game
	{
		if($hand->isValid()){
			$this->hands[]=$hand;
		}
		else{
			throw new \Exception("Invalid hand. Each hand must have atleast one win against!");	
		}
		return $this;
	}
	
	public static function writeLine(string $str)
	{	
		$new_line = php_sapi_name() === 'cli' ? PHP_EOL : "<br>";
		echo $str . $new_line;
	}
}

?>