<?php  
namespace GameContent;
use GameContent\Hand;

class Player
{
	private $name;
	private $score;
	private $hand;

	public function __construct(string $name ='')
	{
		$this->name = $name;
		$this->score = 0;
	}

	public function increaseScore()
	{
		$this->score++;
	}

	public function getScore() : int
	{
		return $this->score;
	}

	public function drawHand(array $hands)
	{
		$this->hand = $hands[rand(0, count($hands)-1)];
	}

	public function getName() : string
	{
		return $this->name;
	}

	public function getHand() : Hand
	{
		return $this->hand;
	}

	public function __toString()
	{
		return $this->name;
	}
}

?>