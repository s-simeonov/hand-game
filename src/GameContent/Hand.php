<?php  
namespace GameContent;

class Hand
{
	private $type;
	private $wins_against = [];

	public function __construct(string $type='')
	{
		$this->type = trim(strtolower($type));
	}

	//two hands cannot win against each other at the same time	
	public function winsAgainst(Hand $hand)
	{
		if( $hand->getType() !== $this->type && !$hand->checkIfWinsAgainst($this) ){
			$this->wins_against[]=$hand->type;
		}
	}

	public function checkIfWinsAgainst(Hand $hand)
	{
		return in_array($hand->getType(), $this->wins_against);
	}

	//every hand must have at least one hand that it wins against to prevent infinte loops
	public function isValid()
	{
		return count($this->wins_against) > 0;
	}

	public function getType()
	{
		return $this->type;
	}

	public function __toString()
	{
		return $this->type;
	}	
}


?>